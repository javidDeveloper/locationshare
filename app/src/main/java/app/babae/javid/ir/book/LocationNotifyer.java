package app.babae.javid.ir.book;

import android.location.Location;

import java.util.Observable;

/**
 * Developed by javid
 * Project : book
 */

public class LocationNotifyer extends Observable {
    private static LocationNotifyer instance;

    public static LocationNotifyer getInstance() {
        if (instance == null)
            instance = new LocationNotifyer();
        return instance;
    }

    public void notifyLocation(Location location) {
        setChanged();
        notifyObservers(location);
    }
}
