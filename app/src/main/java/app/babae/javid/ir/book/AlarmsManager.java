package app.babae.javid.ir.book;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

/**
 * Developed by javid
 * Project : book
 */
public class AlarmsManager {
    private static AlarmsManager instance;
    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;


    public static AlarmsManager getInstance() {
        if (instance == null)
            instance = new AlarmsManager();
        return instance;
    }


    public void setAlarm(/*Context context*/) {
        alarmMgr = (AlarmManager) ContextModel.getContext().getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(ContextModel.getContext(), AlarmReceiver.class);
        alarmIntent = PendingIntent.getBroadcast(ContextModel.getContext(), 0, intent, 0);
        alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() +
                        3 * 1000, alarmIntent); // one min
    }

}
