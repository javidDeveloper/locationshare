package app.babae.javid.ir.book;

import android.app.Application;

import app.babae.javid.ir.book.ContextModel;

/**
 * Developed by javid
 * Project : book
 */
public class BookApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ContextModel.setContext(getApplicationContext());

    }

}
